package com.m46.codechecks.controller;

import com.m46.codechecks.model.dto.CaptchaDto;
import com.m46.codechecks.model.dto.PersonDto;
import com.m46.codechecks.model.dto.EmailRequestDto;
import com.m46.codechecks.service.PersonService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping(path = "/person")
public class PersonController {

    private final PersonService personService;

    @PostMapping(value = "/register", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Object> register(@RequestBody PersonDto personDto) {
       try{
           return ResponseEntity.ok(personService.register(personDto));
       }catch (RuntimeException e){
           return ResponseEntity.status(HttpStatus.LOCKED).body(e);
       }
    }

    @PostMapping("/verify")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Object> verifyEmailCode(@RequestBody EmailRequestDto emailRequestDto) {
        try {
            return ResponseEntity.ok(personService.verifyEmailCode(emailRequestDto));
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.LOCKED).body(e);
        }
    }

    @PostMapping("/captcha")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Object> verifyCaptcha(@RequestBody CaptchaDto answer) {
        try {
            return ResponseEntity.ok(personService.verifyCaptcha(answer));
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.LOCKED).body(e);
        }
    }
}

