package com.m46.codechecks.repository;

import com.m46.codechecks.model.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity,Long> {

    List<ProductEntity> getProductEntitiesByPersonId(Long personId);

//    @Modifying
//    @Query(value = "update products set status='DONE' where person_id=:personId",nativeQuery = true)
//    void updateProductStatusByPersonId(@Param("person_id") Long personId);
}
