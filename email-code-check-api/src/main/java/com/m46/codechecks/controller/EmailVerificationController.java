package com.m46.codechecks.controller;

import com.m46.codechecks.model.EmailVerification;
import com.m46.codechecks.model.VerificationRequest;
import com.m46.codechecks.service.EmailVerificationService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Slf4j
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/email")
public class EmailVerificationController {

    private final EmailVerificationService verificationService;

//    @PostMapping(path = "/{email}/{fio}")
//    @ResponseStatus(HttpStatus.OK)
//    @ApiOperation(value = "", notes = "Utility method to check current state of the email verification process.")
//    public EmailVerificationState getVerificationState(@PathVariable String email,
//                                                       @PathVariable String fio){
//        return verificationService.getVerificationState(email);
//    }

    @PostMapping(path = "/send")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "", notes = "Initial request for email verification. Sends verification code to provided email")
    public void requestEmailVerification(@Validated @RequestBody VerificationRequest verificationRequest) {
        log.info("New verification for {} {}", verificationRequest.getName(), verificationRequest.getEmail());
        verificationService.requestEmailVerification(verificationRequest.getEmail(), verificationRequest.getName());
    }

    @PostMapping(path = "/verify")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "", notes = "Used to verify email by comparing verification code to the one stored in DB.")
    public void verify(@Validated @RequestBody EmailVerification verification) {

        log.info("Verify {} with code '{}'", verification.getEmail(), verification.getVerificationCode());
        verificationService.verify(verification.getEmail(), verification.getVerificationCode());
    }
}
